/** @jsx jsx */

import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { SIGNUP } from "../data/graphql/queries";
import OneColumn from "../components/layouts/OneColumn";
import { jsx, css } from "@emotion/core";
import Router from "next/router";
import Header from "../components/Header";

const SIGNUP_INIT = {
  email: "",
  password: "",
};

const Signup = () => {
  const [values, setValues] = useState(SIGNUP_INIT);
  const [signUp, { error }] = useMutation(SIGNUP);

  const onChange = name => e => {
    setValues({
      ...values,
      [name]: e.target.value,
    });
  };

  const onSubmit = e => {
    e.preventDefault();
    signUp({
      variables: {
        email,
        password,
      },
    }).then(data => Router.push("/profile"));
  };

  const { email, password } = values;

  return (
    <OneColumn>
      <Header title="Sign Up!" />
      <form
        css={formCss}
        onSubmit={onSubmit}
        className="justify-content-center"
      >
        {error && (
          <div className="alert alert-danger">
            {error.graphQLErrors[0].message}
          </div>
        )}
        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <input
            type="email"
            onChange={onChange("email")}
            value={email}
            className="form-control"
            id="exampleInputEmail1"
            aria-describedby="emailHelp"
          />
          <small id="emailHelp" className="form-text text-muted">
            We'll never share your email with anyone else.
          </small>
        </div>
        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>
          <input
            onChange={onChange("password")}
            type="password"
            value={password}
            className="form-control"
            id="exampleInputPassword1"
          />
        </div>
        <div className="form-group form-check">
          <input
            type="checkbox"
            className="form-check-input"
            id="exampleCheck1"
          />
          <label className="form-check-label" htmlFor="exampleCheck1">
            Check me out
          </label>
        </div>
        <button type="submit" className="btn btn-primary">
          Submit
        </button>
      </form>
    </OneColumn>
  );
};

const formCss = () => css`
  margin: 30px auto;
  padding: 20px;
  width: 70%;
  border: 1px solid lightgrey;
  border-radius: 5px;
`;

export default Signup;
