/** @jsx jsx */
import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_TOPIC } from "../../data/graphql/queries";
import OneColumn from "../../components/layouts/OneColumn";
import { jsx, css } from "@emotion/core";
import { TopicList } from "../../components/topic/TopicList";
import { TopicForm } from "../../components/topic/TopicForm";

const Update = props => {
  const { data, error, loading } = useQuery(GET_TOPIC, {
    variables: {
      slug: props.slug,
    },
  });

  return (
    <OneColumn>
      <div css={container}>
        {error && <div>{error.graphQLErrors[0].message}</div>}
        {loading && <div>loading...</div>}
        {!data || data.getTopic.length == 0 ? (
          <div>data not found...</div>
        ) : (
          <TopicForm slug={props.slug} topic={data.getTopic} />
        )}
      </div>
    </OneColumn>
  );
};

const container = () => css`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  border-radius: 5px;
`;

Update.getInitialProps = ctx => {
  return {
    slug: ctx.query.slug,
  };
};

export default Update;
