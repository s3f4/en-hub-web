/** @jsx jsx */
import { useQuery } from "@apollo/react-hooks";
import { GET_TOPIC } from "../../data/graphql/queries";
import OneColumn from "../../components/layouts/OneColumn";
import { jsx, css } from "@emotion/core";
import { TopicContent } from "../../components/topic/TopicContent";
import Header from "../../components/Header";

const Get = props => {
  const { data, errors, loading } = props;

  return (
    <OneColumn>
      <Header title={data && data.getTopic.title} />
      <div css={container}>
        {errors && errors.graphQLErrors && (
          <div>{errors.graphQLErrors[0].message}</div>
        )}
        {loading && <div>loading...</div>}
        {!data ? (
          <div>data not found...</div>
        ) : (
          <TopicContent topic={data.getTopic} />
        )}
      </div>
    </OneColumn>
  );
};

const container = () => css`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  border-radius: 5px;
`;

Get.getInitialProps = async ({ res, req, apolloClient, query }) => {
  const { loading, errors, data } = await apolloClient.query({
    query: GET_TOPIC,
    variables: {
      slug: query.slug,
    },
  });

  if (!data.getTopic) {
    res.statusCode = 404;
    return {};
  }

  return {
    loading,
    data,
    errors,
  };
};

export default Get;
