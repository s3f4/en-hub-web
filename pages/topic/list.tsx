/** @jsx jsx */
import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { LIST_TOPICS } from "../../data/graphql/queries";
import { jsx } from "@emotion/core";
import { TopicList } from "../../components/topic/TopicList";
import { TopicForm } from "../../components/topic/TopicForm";
import Header from "../../components/Header";
import TwoColumn from "../../components/layouts/TwoColumn";

const List = props => {
  const { data, error, loading } = useQuery(LIST_TOPICS, {
    variables: {
      limit: 10,
      offset: 0,
    },
  });

  return (
    <TwoColumn
      main={() => (
        <React.Fragment>
          <Header title="Grammar Topics" />
          <TopicForm />
          {error && error.graphQLErrors && (
            <div>{error.graphQLErrors[0].message}</div>
          )}
          {loading && <div>loading...</div>}
          {!data || data.listTopics.length == 0 ? (
            <div>data not found...</div>
          ) : (
            <TopicList data={data.listTopics} />
          )}
        </React.Fragment>
      )}
      sidebar={() => <div>test</div>}
    />
  );
};

export default List;
