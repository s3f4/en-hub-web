import { useSubscription } from "@apollo/react-hooks";
import { gql } from "apollo-boost";

const USER_SUBSCRIPTION = gql`
  subscription {
    user {
      id
      email
    }
  }
`;
const User = () => {
  const { data, loading } = useSubscription(USER_SUBSCRIPTION);
  return <h4>New comment: {!loading && data && data.user.email}</h4>;
};
export default User;
