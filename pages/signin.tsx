import { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { SIGNIN, PROFILE } from "../data/graphql/queries";
import OneColumn from "../components/layouts/OneColumn";
import Router from "next/router";
import { setAccessToken } from "../lib/accessToken";
import TextInput from "../components/basic/TextInput";
import Header from "../components/Header";
import Button from "../components/basic/Button";

const SIGNIN_INIT = {
  email: "",
  password: "",
};

const Signin = () => {
  const [values, setValues] = useState(SIGNIN_INIT);
  const [signIn, { error }] = useMutation(SIGNIN);

  const onChange = name => e => {
    setValues({
      ...values,
      [name]: e.target.value,
    });
  };

  const onSubmit = async e => {
    e.preventDefault();
    const res = await signIn({
      variables: {
        email,
        password,
      },
      update: (store, { data }) => {
        if (!data) {
          return null;
        }

        store.writeQuery({
          query: PROFILE,
          data: {
            profile: data.signIn.user,
          },
        });
      },
    });
    console.log(res);

    if (res && res.data) {
      setAccessToken(res.data.signIn.accessToken);
    }

    Router.push("/profile");
  };

  const { email, password } = values;

  if (error) {
    return <div>{error.graphQLErrors[0].message}</div>;
  }

  return (
    <OneColumn>
      <Header title="Sign In!" />
      <form onSubmit={onSubmit}>
        {error && (
          <div className="alert alert-danger">
            {error.graphQLErrors[0].message}
          </div>
        )}
        <div className="form-group">
          <label htmlFor="exampleInputEmail1">Email address</label>
          <TextInput
            name="email"
            type="email"
            onChange={onChange("email")}
            value={email}
            validation={{
              min: 5,
              max: 10,
            }}
            id="exampleInputEmail1"
          />
        </div>
        <div className="form-group">
          <label htmlFor="exampleInputPassword1">Password</label>
          <TextInput
            name={"password"}
            onChange={onChange("password")}
            type="password"
            value={password}
            id="exampleInputPassword1"
          />
        </div>
        <Button text="Sign In!"/>
      </form>
    </OneColumn>
  );
};

export default Signin;
