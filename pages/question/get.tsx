/** @jsx jsx */
import React from "react";
import { GET_QUESTION } from "../../data/graphql/queries";
import OneColumn from "../../components/layouts/OneColumn";
import { jsx, css } from "@emotion/core";
import Header from "../../components/Header";
import { QuestionContent } from "../../components/question/QuestionContent";
import TwoColumn from "../../components/layouts/TwoColumn";

const Get = props => {
  const { data, errors, loading } = props;

  return (
    <TwoColumn
      main={() => {
        return (
          <React.Fragment>
            <Header title={data.getQuestion.title} />
            <div css={container}>
              {errors && errors.graphQLErrors && (
                <div>{errors.graphQLErrors[0].message}</div>
              )}
              {loading && <div>loading...</div>}
              {!data ? (
                <div>data not found...</div>
              ) : (
                <QuestionContent question={data.getQuestion} />
              )}
            </div>
          </React.Fragment>
        );
      }}
      sidebar={() => {
        return <div>test</div>;
      }}
    ></TwoColumn>
  );
};

const container = () => css`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  border-radius: 5px;
`;

Get.getInitialProps = async ({ res, req, apolloClient, query }) => {
  const { loading, errors, data } = await apolloClient.query({
    query: GET_QUESTION,
    variables: {
      slug: query.slug,
    },
  });

  if (!data.getQuestion) {
    res.statusCode = 404;
    return {};
  }

  return {
    loading,
    data,
    errors,
  };
};

export default Get;
