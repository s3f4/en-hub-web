/** @jsx jsx */
import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { LIST_QUESTIONS } from "../../data/graphql/queries";
import { jsx, css } from "@emotion/core";
import { QuestionList } from "../../components/question/QuestionList";
import Header from "../../components/Header";
import { QuestionForm } from "../../components/question/QuestionForm";
import TwoColumn from "../../components/layouts/TwoColumn";

const List = props => {
  const { data, error, loading } = useQuery(LIST_QUESTIONS, {
    variables: {
      limit: 10,
      offset: 0,
    },
  });

  console.log(data);

  return (
    <TwoColumn
      main={() => (
        <React.Fragment>
          <Header title="English Questions" />
          <div css={container}>
            <QuestionForm />
            {error && error.graphQLErrors && error.graphQLErrors.length && (
              <div>{error.graphQLErrors[0].message}</div>
            )}
            {loading && <div>loading...</div>}
            {!data || data.listQuestions.length == 0 ? (
              <div>data not found...</div>
            ) : (
              <QuestionList data={data.listQuestions} />
            )}
          </div>
        </React.Fragment>
      )}
      sidebar={() => <div>test</div>}
    />
  );
};

const container = () => css`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  border-radius: 5px;
`;

export default List;
