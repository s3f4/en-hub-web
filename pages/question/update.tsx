/** @jsx jsx */
import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_QUESTION } from "../../data/graphql/queries";
import OneColumn from "../../components/layouts/OneColumn";
import { jsx, css } from "@emotion/core";
import { QuestionForm } from "../../components/question/QuestionForm";

const Update = props => {
  const { data, error, loading } = useQuery(GET_QUESTION, {
    variables: {
      slug: props.slug,
    },
  });

  console.log(error && error.graphQLErrors);
  return (
    <OneColumn>
      <div css={container}>
        {error && error.graphQLErrors && (
          <div>{JSON.stringify(error.graphQLErrors[0])}</div>
        )}
        {loading && <div>loading...</div>}
        {!data || data.getQuestion.length == 0 ? (
          <div>data not found...</div>
        ) : (
          <QuestionForm slug={props.slug} question={data.getQuestion} />
        )}
      </div>
    </OneColumn>
  );
};

const container = () => css`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  border-radius: 5px;
`;

Update.getInitialProps = ctx => {
  return {
    slug: ctx.query.slug,
  };
};

export default Update;
