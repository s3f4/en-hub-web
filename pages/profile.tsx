import React from "react";
import OneColumn from "../components/layouts/OneColumn";
import { useQuery } from "@apollo/react-hooks";
import { PROFILE } from "../data/graphql/queries";

const Profile = () => {
  const { loading, error, data } = useQuery(PROFILE);

  return (
    <OneColumn>
      {error && <div>...error</div>}
      {loading && <div>...loading</div>}
      {JSON.stringify(data && data.profile)}
    </OneColumn>
  );
};

export default Profile;
