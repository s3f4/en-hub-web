/** @jsx jsx */
import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { GET_DICTIONARY } from "../../data/graphql/queries";
import OneColumn from "../../components/layouts/OneColumn";
import { jsx, css } from "@emotion/core";
import { TopicForm } from "../../components/topic/TopicForm";
import { DictionaryForm } from "../../components/dictionary/DictionaryForm";

const Update = props => {
  const { data, error, loading } = useQuery(GET_DICTIONARY, {
    variables: {
      slug: props.slug,
    },
  });

  return (
    <OneColumn>
      <div css={container}>
        {error && <div>{error.graphQLErrors[0].message}</div>}
        {loading && <div>loading...</div>}
        {!data || data.getDictionary.length == 0 ? (
          <div>data not found...</div>
        ) : (
          <DictionaryForm slug={props.slug} dictionary={data.getDictionary} />
        )}
      </div>
    </OneColumn>
  );
};

const container = () => css`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  border-radius: 5px;
`;

Update.getInitialProps = ctx => {
  return {
    slug: ctx.query.slug,
  };
};

export default Update;
