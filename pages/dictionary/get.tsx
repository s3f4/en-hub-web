/** @jsx jsx */
import { GET_DICTIONARY } from "../../data/graphql/queries";
import OneColumn from "../../components/layouts/OneColumn";
import { jsx, css } from "@emotion/core";
import Header from "../../components/Header";
import { DictionaryContent } from "../../components/dictionary/DictionaryContent";

const Get = props => {
  const { data, errors, loading } = props;

  return (
    <OneColumn>
      <Header title={data && data.getDictionary && data.getDictionary.title} />
      <div css={container}>
        {errors && errors.graphQLErrors && (
          <div>{errors.graphQLErrors[0].message}</div>
        )}
        {loading && <div>loading...</div>}
        {!data || !data.getDictionary ? (
          <div>data not found...</div>
        ) : (
          <DictionaryContent dictionary={data.getDictionary} />
        )}
      </div>
    </OneColumn>
  );
};

const container = () => css`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  border-radius: 5px;
`;

Get.getInitialProps = async ({ res, req, apolloClient, query }) => {
  const { data, loading, errors } = await apolloClient.query({
    query: GET_DICTIONARY,
    variables: {
      slug: query.slug,
    },
  });

  if (!data.getDictionary) {
    res.statusCode = 404;
    return {};
  }

  return {
    loading,
    data,
    errors,
  };
};

export default Get;