/** @jsx jsx */
import React, { useEffect, useState } from "react";
import { useQuery, useLazyQuery } from "@apollo/react-hooks";
import {
  LIST_DICTIONARIES,
  LIST_DICTIONARY_TYPES,
} from "../../data/graphql/queries";
import { jsx, css } from "@emotion/core";
import Header from "../../components/Header";
import { DictionaryList } from "../../components/dictionary/DictionaryList";
import { DictionaryForm } from "../../components/dictionary/DictionaryForm";
import TwoColumn from "../../components/layouts/TwoColumn";
import { DictionaryType } from "../../data/model/DictionaryType";
import Link from "next/link";
import { useRouter } from "next/router";
import { Box, Colors, Sizes } from "../../components/style";

const List = props => {
  const router = useRouter();
  const [dictionaryType, setDictionaryType] = useState<any>(null);

  const { data: dictionaryTypes } = useQuery(LIST_DICTIONARY_TYPES);
  const [loadDictionaryies, { data, error, loading }] = useLazyQuery(
    LIST_DICTIONARIES,
    {
      variables: {
        limit: 150,
        offset: 0,
        dictionaryType: dictionaryType,
      },
    },
  );

  useEffect(() => {
    if (router.query.dt) {
      setDictionaryType(router.query.dt);
    }
    loadDictionaryies();
  }, [router.query.dt]);

  return (
    <TwoColumn
      main={() => (
        <React.Fragment>
          <Header title="Dictionary" />
          <div css={container}>
            <DictionaryForm />
            {error && error.graphQLErrors && error.graphQLErrors.length > 0 && (
              <div>{error.graphQLErrors[0].message}</div>
            )}
            {loading && <div>loading...</div>}
            {!data || data.listDictionaries.length == 0 ? (
              <div>data not found...</div>
            ) : (
              <DictionaryList data={data.listDictionaries} />
            )}
          </div>
        </React.Fragment>
      )}
      sidebar={() => (
        <div css={dtContainer}>
          <div css={dtHeader}>Dictionary</div>
          <div css={dtContent}>
            {dictionaryTypes &&
              dictionaryTypes.listDictionaryTypes.map(
                (dictionaryType: DictionaryType) => (
                  <div key={dictionaryType.id}>
                    <Link href={`/dictionary?dt=${dictionaryType.slug}`}>
                      <a>{dictionaryType.name}</a>
                    </Link>
                    <br />
                  </div>
                ),
              )}
          </div>
        </div>
      )}
    ></TwoColumn>
  );
};

const container = () => css`
  margin: 0 auto;
  padding: 20px;
  width: 100%;
  border-radius: 5px;
`;

const dtContainer = css`
  width: 100%;
  border-radius: ${Sizes.borderRadius1};
  border: 1px solid ${Colors.borderPrimary};
  font-size: ${Sizes.fontSizePrimary};
  font-family: sans-serif;
  color: ${Colors.textPrimary};
  ${Box.boxShadow1}
`;

const dtContent = css`
  padding: 1rem;
`;

const dtListItem = css``;

const dtHeader = css`
  background-color: #ddd;
  padding: 0.5rem 1rem;
  font-weight: bold;
  font-size: 2rem;
  text-align: center;
`;

export default List;
