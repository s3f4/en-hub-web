const routes = require("next-routes");
// Name   Page Pattern
module.exports = routes()
  .add("topic/list", "/grammar")
  .add("topic/update", "/grammar/:slug/update")
  .add("topic/get", "/grammar/:slug")

  .add("question/list", "/questions")
  .add("question/update", "/questions/:slug/update")
  .add("question/get", "/questions/:slug")

  .add("dictionary/list", "/dictionary")
  .add("dictionary/update", "/dictionary/:slug/update")
  .add("dictionary/get", "/dictionary/:slug");
