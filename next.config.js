// next.config.js
module.exports = {
  publicRuntimeConfig: {
    APP_NAME: "en-hub",
    API_DEVELOPMENT: "http://localhost:8000/api",
    API_PRODUCTION: "https://en-hub.com/api",
    PRODUCTION: false,
    DOMAIN_DEVELOPMENT: "http://localhost:3000",
    DOMAIN_PRODUCTION: "https://en-hub.com",
  },
};
