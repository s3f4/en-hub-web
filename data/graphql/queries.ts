import { gql } from "apollo-boost";

export const SIGNUP = gql`
  # Write your query or mutation here
  mutation SignUp($email: String!, $password: String!) {
    signUp(email: $email, password: $password) {
      accessToken
    }
  }
`;

export const SIGNIN = gql`
  # Write your query or mutation here
  mutation SignIn($email: String!, $password: String!) {
    signIn(email: $email, password: $password) {
      accessToken
      user {
        email
      }
    }
  }
`;

export const PROFILE = gql`
  query {
    profile {
      email
    }
  }
`;

export const LOGOUT = gql`
  mutation {
    logOut
  }
`;

export const ADD_TOPIC = gql`
  mutation($title: String!, $content: String!) {
    addTopic(topicInput: { title: $title, content: $content }) {
      id
      title
      content
      slug
      createdAt
      updatedAt
    }
  }
`;

export const UPDATE_TOPIC = gql`
  mutation($slug: String!, $title: String!, $content: String!) {
    updateTopic(topicInput: { slug: $slug, title: $title, content: $content }) {
      id
      title
      content
      createdAt
      updatedAt
    }
  }
`;

export const DELETE_TOPIC = gql`
  mutation($slug: String!) {
    deleteTopic(topicInput: { slug: $slug })
  }
`;

export const GET_TOPIC = gql`
  query($slug: String!) {
    getTopic(slug: $slug) {
      id
      title
      content
      slug
      createdAt
      updatedAt
    }
  }
`;

export const LIST_TOPICS = gql`
  query($limit: Int!, $offset: Int!) {
    listTopics(limit: $limit, offset: $offset) {
      id
      title
      content
      slug
      createdAt
      updatedAt
    }
  }
`;

// Questions
export const ADD_QUESTION = gql`
  mutation($title: String!, $content: String!) {
    addQuestion(questionInput: { title: $title, content: $content }) {
      id
      title
      content
      slug
      createdAt
      updatedAt
    }
  }
`;

export const UPDATE_QUESTION = gql`
  mutation($slug: String!, $title: String!, $content: String!) {
    updateQuestion(
      questionInput: { slug: $slug, title: $title, content: $content }
    ) {
      id
      title
      content
      createdAt
      updatedAt
      slug
      answers {
        id
        answer
        createdAt
        updatedAt
      }
    }
  }
`;

export const DELETE_QUESTION = gql`
  mutation($slug: String!) {
    deleteQuestion(questionInput: { slug: $slug })
  }
`;

export const GET_QUESTION = gql`
  query($slug: String!) {
    getQuestion(slug: $slug) {
      id
      title
      content
      slug
      createdAt
      updatedAt
      answers {
        id
        answer
        createdAt
        updatedAt
      }
    }
  }
`;

export const LIST_QUESTIONS = gql`
  query($limit: Int!, $offset: Int!) {
    listQuestions(limit: $limit, offset: $offset) {
      id
      title
      content
      slug
      createdAt
      updatedAt
      answers {
        id
        answer
      }
    }
  }
`;

//Dictionary

export const ADD_DICTIONARY = gql`
  mutation($title: String!, $content: String!) {
    addDictionary(dictionaryInput: { title: $title, content: $content }) {
      id
      title
      content
      slug
      createdAt
      updatedAt
      dictionaryType {
        id
        name
        slug
      }
    }
  }
`;

export const UPDATE_DICTIONARY = gql`
  mutation($slug: String!, $title: String!, $content: String!) {
    updateDictionary(
      dictionaryInput: { slug: $slug, title: $title, content: $content }
    ) {
      id
      title
      content
      slug
      createdAt
      updatedAt
      dictionaryType {
        id
        name
        slug
      }
    }
  }
`;

export const DELETE_DICTIONARY = gql`
  mutation($slug: String!) {
    deleteDictionary(dictionaryInput: { slug: $slug })
  }
`;

export const GET_DICTIONARY = gql`
  query($slug: String!) {
    getDictionary(slug: $slug) {
      id
      title
      content
      slug
      dictionaryType {
        id
        name
        slug
      }
    }
  }
`;

export const LIST_DICTIONARIES = gql`
  query($limit: Int!, $offset: Int!, $dictionaryType: String) {
    listDictionaries(
      limit: $limit
      offset: $offset
      dictionaryType: $dictionaryType
    ) {
      id
      title
      content
      slug
      dictionaryType {
        id
        name
        slug
      }
    }
  }
`;

export const LIST_DICTIONARY_TYPES = gql`
  query {
    listDictionaryTypes {
      id
      slug
      name
    }
  }
`;
