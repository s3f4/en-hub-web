import { BaseEntity } from "./BaseEntity";

export interface Topic extends BaseEntity {
  id: number;
  topicIndex: number;
  title: string;
  content: string;
  slug: string;
}
