import { BaseEntity } from "./BaseEntity";

export interface Answer extends BaseEntity {
  id: number;
  questionId: number;
  answer: string;
}
