import { DictionaryType } from "./DictionaryType";
import { BaseEntity } from "./BaseEntity";
export interface Dictionary extends BaseEntity {
  id: number;
  title: string;
  content: string;
  slug: string;
  dictionaryType: DictionaryType;
}
