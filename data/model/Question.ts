import { Answer } from "./Answer";
import { BaseEntity } from "./BaseEntity";
export interface Question extends BaseEntity {
  id: number;
  title: string;
  content: string;
  slug: string;
  answers?: Answer[];
}
