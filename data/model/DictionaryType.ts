export interface DictionaryType {
  id: number;
  name: string;
  slug: string;
}
