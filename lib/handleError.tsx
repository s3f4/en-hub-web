import React from "react";

export const handleError = error => {
  if (error) {
    if (error.length) {
      return (
        <React.Fragment>
          {error.map(err => {
            return <div>{err.message}</div>;
          })}
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <div>{error.message}</div>
      </React.Fragment>
    );
  }
};
