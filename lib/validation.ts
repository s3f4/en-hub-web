export default interface Validation {
  min?: number;
  max?: number;
  required?: boolean;
}
