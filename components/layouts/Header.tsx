/** @jsx jsx */
import React from "react";
import Link from "next/link";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { PROFILE, LOGOUT } from "../../data/graphql/queries";
import { setAccessToken, getAccessToken } from "../../lib/accessToken";
import Router from "next/router";
import { jsx, css } from "@emotion/core";
import TextInput from "../basic/TextInput";
import { FiSearch } from "react-icons/fi";
import { Borders, Sizes, Box, Colors } from "../style";

const Header = () => {
  const { client, data } = useQuery(PROFILE);

  const [logOut] = useMutation(LOGOUT);

  const onLogOut = () => {
    logOut().then(() => {
      setAccessToken(null);
      client.resetStore();
      Router.push("/signin");
    });
  };

  const search = e => {};
  return (
    <header css={header}>
      <div css={logoDiv}>
        <Link href="/">
          <a>en-hub.com</a>
        </Link>
      </div>

      <div>
        <TextInput
          style={{ float: "left", width: "75%" }}
          name="search"
          type="text"
          value="ss"
          onChange={search}
        />
        <div style={{ float: "left" }}>
          <FiSearch css={searchButton} size="3.9rem" color="#666" />
        </div>
      </div>

      <div css={headerLeft}>
        <Link href="/grammar">
          <a>Grammar</a>
        </Link>
        <Link href="/questions">
          <a>Questions</a>
        </Link>
        <Link href="/dictionary">
          <a>Dictionary</a>
        </Link>
      </div>

      <div css={headerLeft}>
        {getAccessToken() && data && data.profile ? (
          <React.Fragment>
            <div className="mr-2">{data.profile.email}</div>
            <button onClick={onLogOut}>Logout</button>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Link href="/signup">
              <a>Sign up</a>
            </Link>
            <Link href="/signin">
              <a>Sign In</a>
            </Link>
          </React.Fragment>
        )}
      </div>
    </header>
  );
};

const header = css`
  margin: 0 auto;
  padding: 20px 10px;
  height: 10rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: ${Borders.border1};
  box-shadow: red;
`;

const headerLeft = css`
  & > a {
    float: left;
    text-align: center;
    padding: 12px;
    text-decoration: none;
    font-size: 18px;
    line-height: 25px;
    border-radius: 4px;
    font-weight: bold;
  }
`;

const logoDiv = css`
  padding: 10px;
  font-size: 25px;
  font-weight: bold;
`;

const searchButton = css`
  border: 1px solid ${Colors.borderPrimary};
  border-radius: ${Sizes.borderRadius1};
  margin: 0 5px;
  cursor: pointer;
  background-color: yellow;
  ${Box.boxShadow1}
`;

export default Header;
