/** @jsx jsx */
import React from "react";
import Header from "./Header";
import { Footer } from "./Footer";
import { css, jsx } from "@emotion/core";
import { Borders, Sizes } from "../style";

interface TwoColumnProps {
  main: React.FC;
  sidebar: React.FC;
}

const TwoColumn = (props: TwoColumnProps) => {
  const { main: Main, sidebar: Sidebar } = props;

  return (
    <React.Fragment>
      <div css={mainContainer}>
        <div>
          <Header />
        </div>

        <main role="main" css={mainContent}>
          <div css={main}>
            <Main />
          </div>
          <div css={sidebar}>
            <div css={sidebarContent}>
              <Sidebar />
            </div>
          </div>
        </main>

        <Footer />
      </div>
    </React.Fragment>
  );
};

const mainContainer = css`
  margin: 0 auto;
  @media (max-width: 1024px) {
    width: 100%;
  }
  width: 120rem;
`;

const mainContent = css`
  margin: 0 auto;
  display: flex;
  justify-content: center;
`;

const main = css`
  flex: 3;
`;

const sidebar = css`
  flex: 1;
  min-height: 50rem;
`;

const sidebarContent = css`
  margin: 1rem;
  padding: 1rem;
`;

export default TwoColumn;
