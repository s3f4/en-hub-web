/** @jsx jsx */
import React from "react";
import Header from "./Header";
import { Footer } from "./Footer";
import { css, jsx } from "@emotion/core";

const OneColumn = props => {
  return (
    <React.Fragment>
      <div css={mainContainer}>
        <div>
          <Header />
        </div>

        <main role="main" css={mainContent}>
          <div>{props.children}</div>
        </main>

        <Footer />
      </div>
    </React.Fragment>
  );
};

const mainContainer = css`
  margin: 0 auto;
  @media (max-width: 1024px) {
    width: 100%;
  }
  width: 120rem;
`;

const mainContent = css`
  margin: 0 auto;
`;
export default OneColumn;
