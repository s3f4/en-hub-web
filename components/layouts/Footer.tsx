/** @jsx jsx */
import React from "react";
import { jsx, css } from "@emotion/core";
import { Colors } from "../style";

interface Props {}
export const Footer: React.FC<Props> = () => {
  return (
    <footer css={footer}>
      <div css={footerContent}>
        <p>
          <a href="https://en-hub.com">en-hub.com</a>
        </p>
        <p>
          <a href="#">Back to top</a>
        </p>
      </div>
    </footer>
  );
};

const footer = css`
  border-top: 1px solid ${Colors.borderLight};
`;

const footerContent = css`
  margin: 1rem auto;
  text-align: center;
  font-size: 1.3rem;
  color: darkblue;
`;
