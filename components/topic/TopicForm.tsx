import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { ADD_TOPIC, LIST_TOPICS, UPDATE_TOPIC } from "../../data/graphql/queries";
import { handleError } from "../../lib/handleError";
import { Topic } from "../../data/model/Topic";
import Button from "../basic/Button";
import TextInput from "../basic/TextInput";
import { Editor } from "../basic/Editor";
interface Props {
  topic?: Topic;
  slug?: string;
}
export const TopicForm: React.FC<Props> = (props: Props) => {
  const [addTopic] = useMutation(ADD_TOPIC);
  const [updateTopic, { error, loading }] = useMutation(UPDATE_TOPIC);

  const [values, setValues] = useState({
    title: props.topic ? props.topic.title : "",
    content: props.topic ? props.topic.content : "",
  });

  const addTopicSubmit = e => {
    e.preventDefault();
    addTopic({
      variables: {
        title: title,
        content: content,
      },
      update: (store, { data: addTopic }) => {
        const { listTopics }: any = store.readQuery({
          query: LIST_TOPICS,
          variables: {
            limit: 10,
            offset: 0,
          },
        });

        store.writeQuery({
          query: LIST_TOPICS,
          variables: {
            limit: 10,
            offset: 0,
          },
          data: {
            listTopics: [addTopic.addTopic, ...listTopics],
          },
        });
      },
    });
  };

  const updateTopicSubmit = e => {
    e.preventDefault();
    updateTopic({
      variables: {
        slug: props.slug,
        title: title,
        content: content,
      },
      update: (store, { data: addTopic }) => {
        const { listTopics }: any = store.readQuery({
          query: LIST_TOPICS,
          variables: {
            limit: 10,
            offset: 0,
          },
        });

        store.writeQuery({
          query: LIST_TOPICS,
          variables: {
            limit: 10,
            offset: 0,
          },
          data: {
            listTopics: [addTopic.addTopic, ...listTopics],
          },
        });
      },
    });
  };

  const onChange = name => e => {
    setValues({
      ...values,
      [name]: typeof e === "string" ? e : e.target.value,
    });
  };

  const { title, content } = values;

  return (
    <React.Fragment>
      {loading && <div className="alert">...Loading</div>}
      {error && <div className="alert">{handleError(error)}</div>}
      <form onSubmit={props.topic ? updateTopicSubmit : addTopicSubmit}>
        <div>
          <label htmlFor="title">Title</label>
          <TextInput
            name="title"
            value={title}
            type="text"
            onChange={onChange("title")}
            id="title"
          />
        </div>
        <div>
          <label htmlFor="title">Content</label>
          <Editor onChange={onChange("content")} value={content} />
        </div>
        <Button text={!props.topic ? "Add" : "Update"} />
      </form>
    </React.Fragment>
  );
};
