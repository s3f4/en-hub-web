import React from "react";
import { Topic } from "../../data/model/Topic";
import Link from "next/link";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_TOPIC } from "../../data/graphql/queries";
import Router from "next/router";
import Button from "../basic/Button";
import renderHTML from "react-render-html";
interface Props {
  topic: Topic;
}

export const TopicContent: React.FC<Props> = props => {
  const { topic } = props;

  const [deleteTopic, { data, error, loading }] = useMutation(DELETE_TOPIC);

  const onDelete = () => {
    deleteTopic({
      variables: {
        slug: topic.slug,
      },
    }).then(data => Router.push("/grammar"));
  };

  return (
    <React.Fragment>
      <div className="card mt-2" style={{ width: "100%" }}>
        <div className="card-body">
          <h5 className="card-title">
            <Link href={`/grammar/${topic.slug}`} as={`/grammar/${topic.slug}`}>
              <a>{topic.title}</a>
            </Link>
          </h5>
          <div>{renderHTML(topic.content)}</div>
          <Link
            href={`/grammar/${topic.slug}/update`}
            as={`/grammar/${topic.slug}/update`}
          >
            <a>Update</a>
          </Link>
          <Button onClick={onDelete} text="Delete" />
        </div>
      </div>
    </React.Fragment>
  );
};
