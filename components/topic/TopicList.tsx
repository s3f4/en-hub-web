/** @jsx jsx */
import React from "react";
import { Topic } from "../../data/model/Topic";
import { jsx, css } from "@emotion/core";
import Link from "next/link";
import { Sizes, Borders, Colors } from "../style";
import moment from "moment";
interface Props {
  data: Topic[];
}
export const TopicList: React.FC<Props> = (props: Props) => {
  return (
    <div css={css``}>
      {props.data &&
        props.data.map(topic => (
          <Link key={topic.id} href={`grammar/${topic.slug}`}>
            <a css={link}>
              <div css={container}>
                <div css={listItem}>
                  <div css={title}>{topic.title}</div>
                  <div css={topicSmall}>
                    {moment(topic.createdAt).fromNow()}
                  </div>
                </div>
              </div>
            </a>
          </Link>
        ))}
    </div>
  );
};

const container = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-flow: row nowrap;
  padding: 1rem;
  border: ${Borders.border1};
  border-radius: ${Sizes.borderRadius1};
  height: 8rem;
  background-color: ${Colors.qBoxBackgroundColor};
  margin-bottom: 1rem;
`;

const title = css`
  color: darkblue;
`;

const link = css`
  width: 100%;
  cursor: pointer;
`;

const listItem = css`
  flex: 8;
  height: 100%;
  padding-left: 1rem;
`;

const sub = css`
  font-size: ${Sizes.smallText};
`;

const topicSmall = css`
  ${sub}
  text-align: right;
`;
