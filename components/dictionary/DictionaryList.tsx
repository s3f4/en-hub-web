/** @jsx jsx */
import React from "react";
import { jsx, css } from "@emotion/core";
import { Dictionary } from "../../data/model/Dictionary";
import { DictionaryCard } from "./DictionaryCard";
interface DictionaryListProps {
  data: Dictionary[];
}

export const DictionaryList: React.FC<DictionaryListProps> = (
  props: DictionaryListProps,
) => {
  return (
    <div css={c}>
      {props.data &&
        props.data.map(dictionary => {
          return <DictionaryCard key={dictionary.id} dictionary={dictionary} />;
        })}
    </div>
  );
};

const c = css`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-wrap: wrap;
  width: 100%;
`;
