import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import {
  ADD_DICTIONARY,
  LIST_DICTIONARIES,
  UPDATE_DICTIONARY,
} from "../../data/graphql/queries";
import { handleError } from "../../lib/handleError";
import { Dictionary } from "../../data/model/Dictionary";
import TextInput from "../basic/TextInput";
import Button from "../basic/Button";

interface Props {
  dictionary?: Dictionary;
  slug?: string;
}
export const DictionaryForm: React.FC<Props> = (props: Props) => {
  const [showForm, setShowForm] = React.useState(false);
  const [addDictionary] = useMutation(ADD_DICTIONARY);
  const [updateDictionary, { error, loading }] = useMutation(UPDATE_DICTIONARY);

  const [values, setValues] = useState({
    title: props.dictionary ? props.dictionary.title : "",
    content: props.dictionary ? props.dictionary.content : "",
  });

  const addDictionarySubmit = e => {
    e.preventDefault();
    addDictionary({
      variables: {
        title: title,
        content: content,
      },
      update: (store, { data: addDictionary }) => {
        const { listDictionaries }: any = store.readQuery({
          query: LIST_DICTIONARIES,
          variables: {
            limit: 10,
            offset: 0,
          },
        });

        store.writeQuery({
          query: LIST_DICTIONARIES,
          variables: {
            limit: 10,
            offset: 0,
          },
          data: {
            listDictionaries: [
              addDictionary.addDictionary,
              ...listDictionaries,
            ],
          },
        });
      },
    });
  };

  const updateDictionarySubmit = e => {
    e.preventDefault();
    updateDictionary({
      variables: {
        slug: props.slug,
        title: title,
        content: content,
      },
    });
  };

  const onChange = name => e => {
    setValues({
      ...values,
      [name]: e.target.value,
    });
  };
  const { title, content } = values;
  return (
    <React.Fragment>
      {loading && <div className="alert">...Loading</div>}
      {error && <div className="alert">{handleError(error)}</div>}
      <form
        onSubmit={
          props.dictionary ? updateDictionarySubmit : addDictionarySubmit
        }
      >
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <TextInput
            name="title"
            value={title}
            type="text"
            onChange={onChange("title")}
            id="title"
          />
        </div>
        <div className="form-group">
          <label htmlFor="title">Content</label>
          <TextInput
            textArea={true}
            name="content"
            onChange={onChange("content")}
            value={content}
            id="content"
          />
        </div>
        <Button text={!props.dictionary ? "Add" : "Update"} />
      </form>
    </React.Fragment>
  );
};
