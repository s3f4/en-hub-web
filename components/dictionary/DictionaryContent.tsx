import React from "react";
import Link from "next/link";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_DICTIONARY } from "../../data/graphql/queries";
import Router from "next/router";
import { Dictionary } from "../../data/model/Dictionary";

interface Props {
  dictionary: Dictionary;
}

export const DictionaryContent: React.FC<Props> = props => {
  const { dictionary } = props;

  const [deleteDictionary, { data, error, loading }] = useMutation(
    DELETE_DICTIONARY,
  );

  const onDelete = () => {
    deleteDictionary({
      variables: {
        slug: dictionary.slug,
      },
    }).then(data => Router.push("/dictionary"));
  };

  return (
    <React.Fragment>
      <div>
        <div>
          <h5 className="card-title">
            <Link href={`/dictionary/${dictionary.slug}`}>
              <a>{dictionary.title}</a>
            </Link>
          </h5>
          <p>{dictionary.content}</p>
          <Link href={`/dictionary/${dictionary.slug}/update`}>
            <a>Update</a>
          </Link>
          <button className="btn btn-primary ml-2" onClick={onDelete}>
            Delete
          </button>
        </div>
      </div>
    </React.Fragment>
  );
};
