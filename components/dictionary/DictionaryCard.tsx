/** @jsx jsx */
import { Dictionary } from "../../data/model/Dictionary";
import { jsx, css } from "@emotion/core";
import Link from "next/link";
import Button from "../basic/Button";
import { Borders, Sizes, Box } from "../style";
import { DELETE_DICTIONARY } from "../../data/graphql/queries";
import { useMutation } from "@apollo/react-hooks";
import Router from "next/router";

interface DictionaryCardProps {
  dictionary: Dictionary;
}

export const DictionaryCard: React.FC<DictionaryCardProps> = (
  props: DictionaryCardProps,
) => {
  const { dictionary } = props;

  const [deleteDictionary, { data, error, loading }] = useMutation(
    DELETE_DICTIONARY,
  );

  const onDelete = () => {
    deleteDictionary({
      variables: {
        slug: dictionary.slug,
      },
    }).then(data => Router.push("/dictionary"));
  };

  return (
    <div css={card}>
      <h5 css={cardHeader}>
        <Link href={`/dictionary/${dictionary.slug}`}>
          <a>{dictionary.title}</a>
        </Link>
      </h5>
      <div css={cardContent}>
        <p>{dictionary.content}</p>
      </div>
    </div>
  );
};

const card = css`
  width: 27.6rem;
  margin: 0.5rem 0.5rem;
  border: ${Borders.border1};
  border-radius: ${Sizes.borderRadius1};
  ${Box.boxShadow1}
`;

const cardHeader = css`
  background-color: #ddd;
  padding: 0.5rem 1rem;
  font-weight: bold;
  font-size: 2rem;
  text-align: center;
`;

const cardContent = css`
  margin: 1rem 1.4rem;
`;
