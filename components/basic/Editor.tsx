import React from "react";
import dynamic from "next/dynamic";
import { QuillFormats, QuillModules } from "../../lib/quill";
const ReactQuill = dynamic(() => import("react-quill"), { ssr: false });

export const Editor = props => {
  const { value, onChange } = props;
  return (
    <ReactQuill
      modules={QuillModules}
      formats={QuillFormats}
      value={value}
      onChange={onChange}
      style={{
        height: "350px",
        marginBottom: "40px",
      }}
    />
  );
};
