/** @jsx jsx */
import Validation from "../../lib/validation";
import { useEffect, useState, ChangeEvent } from "react";
import { jsx, css } from "@emotion/core";
import { Colors, Sizes, Box } from "../style";

interface TextInputProps {
  name: string;
  type?: string;
  value: string;
  validation?: Validation;
  onChange: (
    event: ChangeEvent<HTMLInputElement> | ChangeEvent<HTMLTextAreaElement>,
  ) => void;
  id?: string;
  style?: any;
  textArea?: boolean;
}

const TextInput = (props: TextInputProps) => {
  const [valid, setValid] = useState(false);

  const {
    name,
    type,
    value,
    validation,
    onChange,
    id,
    style,
    textArea,
  } = props;

  const isValid = () => {
    return !(
      (validation.min && value.length < validation.min) ||
      (validation.max && value.length > validation.max) ||
      (validation.required && value.length == 0)
    );
  };

  useEffect(() => {
    if (validation) {
      setValid(isValid());
    } else {
      setValid(true);
    }
  }, [props]);

  if (textArea) {
    return (
      <textarea
        css={inputCSS(valid, textArea)}
        id={id}
        onChange={onChange}
        name={name}
        value={value}
        style={style}
      />
    );
  } else {
    return (
      <input
        css={inputCSS(valid)}
        id={id}
        onChange={onChange}
        name={name}
        type={type || "text"}
        value={value}
        style={style}
      />
    );
  }
};

const inputCSS = (valid: boolean, textArea?: boolean) => css`
  padding: 1rem 1rem;
  width: 100%;
  height: ${textArea ? Sizes.textAreaHeight : Sizes.inputHeight};
  border-radius: ${Sizes.borderRadius1};
  border: 1px solid ${valid ? Colors.borderPrimary : Colors.borderInvalid};
  font-size: ${Sizes.fontSizePrimary};
  font-family: sans-serif;
  color: ${Colors.textPrimary};
  ${Box.boxShadow1}
  &:focus {
    background-color: ${Colors.inputFocus};
    outline: none;
  }
`;

export default TextInput;
