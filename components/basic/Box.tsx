/** @jsx jsx */
import React from "react";
import { jsx, css } from "@emotion/core";
const Box = ({ children }) => {
  return (
    <React.Fragment>
      <div css={box}>{children}</div>
    </React.Fragment>
  );
};

const box = css`
  margin: 10px auto;
  width: 100%;
  padding: 1rem 3rem;
  border: 1px solid #d6d6d6;
  border-radius: 0.4rem;
  box-shadow: 0.1rem 0.2rem 0.5rem #d6d6d6;
`;
export default Box;
