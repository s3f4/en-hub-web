/** @jsx jsx */
import React from "react";
import { jsx, css } from "@emotion/core";
import { Colors, Sizes, Borders, Box } from "../style";

interface ButtonProps {
  text?: string;
  onClick?: any;
}
const Button = (props: ButtonProps) => {
  const { text, onClick } = props;
  return (
    <input
      css={style}
      type="submit"
      value={text ? text : "submit"}
      onClick={onClick}
    />
  );
};

const style = css`
  padding: 0.5rem 1rem;
  height: ${Sizes.inputHeight};
  border: 2px solid ${Colors.borderPrimary};
  border-radius: ${Sizes.borderRadius1};
  color: #665;
  font-weight: bold;
  background-color: ${Colors.buttonPrimary};
  font-size: ${Sizes.fontSizePrimary};
  margin: 1rem 0;
  cursor: pointer;
  &:hover {
    transition: 0.2s ease-out;
    background-color: yellow;
    ${Box.boxShadow1}
  }
`;

export default Button;
