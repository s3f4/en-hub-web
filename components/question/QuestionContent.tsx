/** @jsx jsx */
import React from "react";
import Link from "next/link";
import { useMutation } from "@apollo/react-hooks";
import { DELETE_QUESTION } from "../../data/graphql/queries";
import Router from "next/router";
import { Question } from "../../data/model/Question";
import { Answer } from "../../data/model/Answer";
import { jsx, css } from "@emotion/core";

interface Props {
  question: Question;
}

export const QuestionContent: React.FC<Props> = props => {
  const { question } = props;

  const [deleteQuestion, { data, error, loading }] = useMutation(
    DELETE_QUESTION,
  );

  const onDelete = () => {
    deleteQuestion({
      variables: {
        slug: question.slug,
      },
    }).then(data => Router.push("/questions"));
  };

  return (
    <React.Fragment>
      <div>
        <div>
          <h1>
            <Link href={`/questions/${question.slug}`}>
              <a css={contentTitle}>{question.title}</a>
            </Link>
          </h1>
          <p css={content}>{question.content}</p>
          <Link href={`/questions/${question.slug}/update`}>
            <a>Update</a>
          </Link>
          <button onClick={onDelete}>Delete</button>
          {question.answers
            ? question.answers.map((answer: Answer) => {
                return <div>{answer.answer}</div>;
              })
            : ""}
        </div>
      </div>
    </React.Fragment>
  );
};

const contentTitle = css`
  text-align: center;
`;

const content = css`
  text-align: center;
`;
