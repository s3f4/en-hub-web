import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import {
  ADD_QUESTION,
  UPDATE_QUESTION,
  LIST_QUESTIONS,
} from "../../data/graphql/queries";
import { handleError } from "../../lib/handleError";
import { Question } from "../../data/model/Question";
import Button from "../basic/Button";
import TextInput from "../basic/TextInput";

interface Props {
  question?: Question;
  slug?: string;
}
export const QuestionForm: React.FC<Props> = (props: Props) => {
  const [addQuestion] = useMutation(ADD_QUESTION);
  const [updateQuestion, { error, loading }] = useMutation(UPDATE_QUESTION);

  const [values, setValues] = useState({
    title: props.question ? props.question.title : "",
    content: props.question ? props.question.content : "",
  });

  const addQuestionSubmit = e => {
    e.preventDefault();
    addQuestion({
      variables: {
        title: title,
        content: content,
      },
      update: (store, { data: addQuestion }) => {
        const { listQuestions }: any = store.readQuery({
          query: LIST_QUESTIONS,
          variables: {
            limit: 10,
            offset: 0,
          },
        });

        store.writeQuery({
          query: LIST_QUESTIONS,
          variables: {
            limit: 10,
            offset: 0,
          },
          data: {
            listQuestions: [addQuestion.addQuestion, ...listQuestions],
          },
        });
      },
    });
  };

  const updateQuestionSubmit = e => {
    e.preventDefault();
    updateQuestion({
      variables: {
        slug: props.slug,
        title: title,
        content: content,
      },
      update: (store, { data: addQuestion }) => {
        const { listQuestions }: any = store.readQuery({
          query: LIST_QUESTIONS,
          variables: {
            limit: 10,
            offset: 0,
          },
        });

        store.writeQuery({
          query: LIST_QUESTIONS,
          variables: {
            limit: 10,
            offset: 0,
          },
          data: {
            listQuestions: [addQuestion.addQuestion, ...listQuestions],
          },
        });
      },
    });
  };

  const onChange = name => e => {
    setValues({
      ...values,
      [name]: e.target.value,
    });
  };
  const { title, content } = values;
  return (
    <React.Fragment>
      {loading && <div className="alert">...Loading</div>}
      {error && <div className="alert">{handleError(error)}</div>}
      <form
        onSubmit={props.question ? updateQuestionSubmit : addQuestionSubmit}
      >
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <TextInput
            name="title"
            value={title}
            type="text"
            onChange={onChange("title")}
            id="title"
          />
        </div>
        <div className="form-group">
          <label htmlFor="title">Content</label>
          <TextInput
            textArea={true}
            name="content"
            onChange={onChange("content")}
            value={content}
            id="content"
          />
        </div>
        <Button text={!props.question ? "Add" : "Update"} />
      </form>
    </React.Fragment>
  );
};
