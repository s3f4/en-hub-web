/** @jsx jsx */
import React from "react";
import { jsx, css } from "@emotion/core";
import { Question } from "../../data/model/Question";
import { Borders, Sizes, Colors } from "../style";
import Link from "next/link";
import moment from "moment";
interface Props {
  data: Question[];
}

export const QuestionList: React.FC<Props> = (props: Props) => {
  return (
    <div css={css``}>
      {props.data &&
        props.data.map(question => (
          <Link key={question.id} href={`questions/${question.slug}`}>
            <a css={questionLink}>
              <div css={questionContainer}>
                <div css={views}>
                  <div> 55</div>
                  <div css={sub}>views</div>
                </div>
                <div css={answers}>
                  <div>{question.answers.length}</div>
                  <div css={sub}>Answers</div>
                </div>
                <div css={questionListItem}>
                  <div css={title}>{question.title}</div>
                  <div css={questionSmall}>
                    {moment(question.createdAt).fromNow()}
                  </div>
                </div>
              </div>
            </a>
          </Link>
        ))}
    </div>
  );
};

const sub = css`
  font-size: ${Sizes.smallText};
`;

const questionContainer = css`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-flow: row nowrap;
  padding: 1rem;
  border: ${Borders.border1};
  border-radius: ${Sizes.borderRadius1};
  height: 8rem;
  background-color: ${Colors.qBoxBackgroundColor};
  margin-bottom: 1rem;
`;

const flex = css`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-around;
  text-align: center;
  padding: 1rem;
`;

const views = css`
  flex: 1;
  ${flex}
`;

const answers = css`
  flex: 1;
  height: 100%;
  ${flex}
`;

const title = css`
  color: darkblue;
`;

const questionLink = css`
  width: 100%;
  cursor: pointer;
`;

const questionListItem = css`
  flex: 8;
  height: 100%;
  padding-left: 1rem;
`;

const questionSmall = css`
  ${sub}
  text-align: right;
`;
