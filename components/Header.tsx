import React from "react";
import Head from "next/head";

interface Props {
  title: string;
}
const Header = (props: Props) => {
  return (
    <Head>
      <link
        href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap"
        rel="stylesheet"
      />
      <link rel="shortcut icon" href="static/img/favicon.png" />
      <title>en-hub.com | {props.title}</title>
    </Head>
  );
};

export default Header;
